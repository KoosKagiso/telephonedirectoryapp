var myApp = angular.module('myApp', []);

myApp.controller('myController', function($scope){
	console.log("In myContoller...");
	
	$scope.newUser = {};
	$scope.checkedUser = {};
	$scope.message = "";

	$scope.users = 
	[
		{FNAME:"Kagiso Koos ",LNAME:"Llale",TELEPHONE:"064 685 2570"}
	];
	$scope.saveUser = function(){
		$scope.users.push($scope.newUser);
		$scope.newUser = {};
		$scope.message = "Person added Successful";
	};

	$scope.selectUser = function(user){
		console.log(user);
		$scope.clickedUser = user;
	};

	$scope.updateUser = function(user){

	};

	$scope.deleteUser = function(){
		$scope.users.splice($scope.users.indexOf($scope.clickedUser), 1);
	};
});
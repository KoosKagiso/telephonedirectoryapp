## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)

## General info
This project its a Telephone directory online application where you add a person 
* with his name,
* surname, 
* and telephone number.

Once the person has been added I want to be able to
* update the persons details
* as well as delete the person
	
## Technologies
Project is created with:
* Angular Js ,
* Bootstrap 3,
* Jquery,
* Font Awesome

## Setup
To run this project, run it locally

```
$ cd ../Telephone Directory App
$ run index.html
$ and you good to go
```